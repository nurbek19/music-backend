const mongoose = require('mongoose');
const config = require('./config');

const Artist = require('./models/Artist');
const Album = require('./models/Album');
const Track = require('./models/Track');
const User = require('./models/User');

mongoose.connect(config.db.url + '/' + config.db.name);

const db = mongoose.connection;

db.once('open', async () => {
    try {
        await db.dropCollection('users');
        await db.dropCollection('artists');
        await db.dropCollection('albums');
        await db.dropCollection('tracks');
        await db.dropCollection('trackhistories');
    } catch (e) {
        console.log('Collections were not present, skipping drop...');
    }

    await User.create({
        username: 'Maks',
        password: '123',
        role: 'user'
    }, {
        username: 'admin',
        password: '123',
        role: 'admin'
    });

    const [Beyonce, Adele] = await Artist.create({
        name: 'Beyonce',
        description: 'She is very popular artist',
        image: 'beyonce.jpg',
        published: true
    }, {
        name: 'Adele',
        description: 'She is very famous artist',
        image: 'adele.jpeg',
        published: false
    });

    const [Lemonade, MoreOnly, GreatesHits] = await Album.create({
        name: 'Lemonade',
        artist: Beyonce._id,
        releaseYear: '2015',
        image: 'lemonade.png',
        published: true
    }, {
        name: 'MoreOnly',
        artist: Beyonce._id,
        releaseYear: '2017',
        image: 'more-only.jpg',
        published: false
    }, {
        name: 'GreatestHits',
        artist: Adele._id,
        releaseYear: '2004',
        image: 'greatest-hits.jpg',
        published: false
    });

    await Track.create({
        title: 'Skyfall',
        album: GreatesHits._id,
        duration: '3:58',
        published: false
    }, {
        title: 'Rolling in the deep',
        album: GreatesHits._id,
        duration: '4:12',
        published: true
    }, {
        title: 'Hold Up',
        album: Lemonade._id,
        duration: '3:56',
        published: false
    }, {
        title: "Don't hurt yourself",
        album: Lemonade._id,
        duration: '4:14',
        published: true
    }, {
        title: "Ring Off",
        album: MoreOnly._id,
        duration: '4:14',
        published: false
    });

    db.close();
});
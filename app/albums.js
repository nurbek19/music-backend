const express = require('express');
const multer = require('multer');
const router = express.Router();
const path = require('path');
const nanoid = require('nanoid');
const Album = require('../models/Album');
const config = require('../config');

const auth = require('../middleware/auth');
const permit = require('../middleware/permit');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});


const createRouter = () => {
    router.get('/', (req, res) => {
        const artistId = req.query.artist;

        if (artistId) {
            Album.find({artist: artistId})
                .then(result => res.send(result))
                .catch(() => res.sendStatus(500));

        } else {
            Album.find()
                .then(results => res.send(results))
                .catch(() => res.sendStatus(500));
        }
    });

    router.get('/:id', (req, res) => {
        Album.findOne({_id: req.params.id}).populate('artist')
            .then(result => {
                if (result) res.send(result);
                else res.sendStatus(404);
            })
            .catch(() => res.sendStatus(500));
    });

    router.post('/', [auth, permit('user', 'admin'), upload.single('image')], (req, res) => {
        const album = req.body;

        if (req.file) {
            album.image = req.file.filename;
        } else {
            album.image = null;
        }

        const albumData = new Album(album);

        albumData.save()
            .then(result => res.send(result))
            .catch(error => res.status(400).send(error));
    });


    router.delete('/:id', [auth, permit('admin')], (req, res) => {
        Album.deleteOne({_id: req.params.id})
            .then(result => res.send(result))
            .catch(error => res.status(400).send(error))
    });

    router.put('/:id', [auth, permit('admin')], async (req, res) => {
        const album = await  Album.findOne({_id: req.params.id});

        album.published = true;

        album.save()
            .then(album => res.send(album))
            .catch(error => res.status(400).send(error));
    });

    return router;
};

module.exports = createRouter;
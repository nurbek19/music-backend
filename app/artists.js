const express = require('express');
const multer = require('multer');
const router = express.Router();
const path = require('path');
const nanoid = require('nanoid');
const Artist = require('../models/Artist');
const config = require('../config');

const auth = require('../middleware/auth');
const permit = require('../middleware/permit');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});


const createRouter = () => {
    router.get('/', (req, res) => {
        Artist.find()
            .then(results => res.send(results))
            .catch(() => res.sendStatus(500));
    });

    router.post('/', [auth, permit('user', 'admin'), upload.single('image')], (req, res) => {
        const artist = req.body;

        if (req.file) {
            console.log(req.file);
            artist.image = req.file.filename;
        } else {
            artist.image = null;
        }

        const artistData = new Artist(artist);

        artistData.save()
            .then(result => res.send(result))
            .catch(error => res.status(400).send(error));
    });


    router.delete('/:id', [auth, permit('admin')], (req, res) => {
        Artist.deleteOne({_id: req.params.id})
            .then(result => res.send(result))
            .catch(error => res.status(400).send(error))
    });

    router.put('/:id', [auth, permit('admin')], async (req, res) => {
        const artist = await  Artist.findOne({_id: req.params.id});

        artist.published = true;

        artist.save()
            .then(artist => res.send(artist))
            .catch(error => res.status(400).send(error));
    });

    return router;
};

module.exports = createRouter;
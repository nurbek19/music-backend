const express = require('express');
const User = require('../models/User');
const TrackHistory = require('../models/TrackHistory');

const auth = require('../middleware/auth');
const permit = require('../middleware/permit');

const createRouter = () => {
    const router = express.Router();

    router.post('/', auth, async (req, res) => {
        // const token = req.get('Token');
        const trackHistory = new TrackHistory(req.body);
        //
        // if (!token) {
        //     return res.status(401).send({error: 'Token is not found'});
        // }
        //
        // const user = await User.findOne({token: token});
        //
        // if (!user) {
        //     return res.status(401).send({error: 'User is not authorized'});
        // }


        trackHistory.user = req.user._id;
        trackHistory.datetime = new Date().toISOString();

        await trackHistory.save();

        return res.send(trackHistory);
    });

    router.get('/', auth, async (req, res) => {
        // const token = req.get('Token');
        //
        // if (!token) {
        //     return res.status(401).send({error: 'Token is not found'});
        // }
        //
        // const user = await User.findOne({token: token});
        //
        // if (!user) {
        //     return res.status(401).send({error: 'User is not authorized'});
        // }

        TrackHistory.find({user: req.user._id}).populate({
            path: 'track', populate: {path: 'album', populate: {path: 'artist'}}
        })
            .then(result => res.send(result.sort((a, b) => {
                return new Date(a.datetime) - new Date(b.datetime);
            }).reverse()))
            .catch(() => res.sendStatus(500));
    });

    return router;
};

module.exports = createRouter;
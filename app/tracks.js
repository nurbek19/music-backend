const express = require('express');
const router = express.Router();
const Track = require('../models/Track');

const auth = require('../middleware/auth');
const permit = require('../middleware/permit');


const createRouter = () => {
    router.get('/', (req, res) => {
        const albumId = req.query.album;

        if (albumId) {
            Track.find({album: albumId})
                .then(result => res.send(result.sort((a, b) => {return a.number - b.number})))
                .catch(() => res.sendStatus(500));

        } else {
            Track.find().populate('album')
                .then(results => res.send(results.sort((a, b) => {return a.number - b.number})))
                .catch(() => res.sendStatus(500));
        }
    });

    router.get('/:id', (req, res) => {
        Track.findOne({_id: req.params.id}).populate('album')
            .then(result => {
                if (result) res.send(result);
                else res.sendStatus(404);
            })
            .catch(() => res.sendStatus(500));
    });

    router.post('/', [auth, permit('user', 'admin')], (req, res) => {
        const track = new Track(req.body);

        track.save()
            .then(result => res.send(result))
            .catch(error => res.status(400).send(error));
    });

    router.delete('/:id', [auth, permit('admin')], (req, res) => {
        Track.deleteOne({_id: req.params.id})
            .then(result => res.send(result))
            .catch(error => res.status(400).send(error))
    });

    router.put('/:id', [auth, permit('admin')], async (req, res) => {
        const track = await  Track.findOne({_id: req.params.id});

        track.published = true;

        track.save()
            .then(track => res.send(track))
            .catch(error => res.status(400).send(error));
    });

    return router;
};

module.exports = createRouter;